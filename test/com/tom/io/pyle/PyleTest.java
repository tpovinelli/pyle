package com.tom.io.pyle;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;

public class PyleTest {

  @Test
  public void testReadText() throws IOException {
    Pyle pyle = new Pyle("testfiles/readme.txt", FileMode.READ);
    String text = pyle.read();
    Assertions.assertEquals("this is a readme file that is being used to test Pyle\n" +
        "here is line 2\n" +
        "this is another line there is a lot happening", text);
    pyle.close();
  }

  @Test
  public void testSkipRead() throws IOException {
    Pyle pyle = new Pyle("testfiles/read-skip.txt", FileMode.READ);
    char[] buf = new char[10];
    pyle.read(buf, 0, 10);
    System.out.println(Arrays.toString(buf));
    pyle.read(null, 0, 12);
    pyle.read(buf, 0, 10);
    System.out.println(Arrays.toString(buf));
    pyle.close();
  }

  @Test
  public void testReadBlank() throws IOException {
    Pyle pyle1 = new Pyle("testfiles/blank.txt", FileMode.READ);
    String text2 = pyle1.read();
    Assertions.assertEquals("", text2);
    pyle1.close();
  }

  @Test
  public void testReadLineText() throws IOException {
    Pyle pyle = new Pyle("testfiles/readme.txt", FileMode.READ);
    String text = pyle.readLine();
    Assertions.assertEquals("this is a readme file that is being used to test Pyle", text);
    pyle.close();
  }

  @Test
  public void testReadLineBlank() throws IOException {
    Pyle pyle1 = new Pyle("testfiles/blank.txt", FileMode.READ);
    String text2 = pyle1.readLine();
    Assertions.assertEquals("", text2);
    pyle1.close();
  }

  @Test
  public void testWriteStringText() throws IOException {
    Pyle p = new Pyle("testfiles/writeme.txt", FileMode.WRITE);
    p.write("Hello, world");
    p.close();

    p = new Pyle("testfiles/writeme.txt", FileMode.READ);
    Assertions.assertEquals("Hello, world", p.read());
    p.close();
  }

  @Test
  public void testTruncateText() throws IOException {
    Pyle p = new Pyle("testfiles/writeme.txt", FileMode.WRITE);
    p.close();
    p = new Pyle("testfiles/writeme.txt", FileMode.READ);
    Assertions.assertEquals("", p.read());
    p.close();
  }

  @Test
  public void testReadPlusReadFirstText() throws IOException {
    Pyle p = new Pyle("testfiles/simple.txt", FileMode.READ_PLUS);
    String text = p.readLine();
    p.write("Ending Text");
    Assertions.assertEquals("this is just a simple file with some text in it.", text);
    String more = p.read();
    Assertions.assertEquals("", more);
    p.close();
  }
}
