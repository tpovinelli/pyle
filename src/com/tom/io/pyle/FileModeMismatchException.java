package com.tom.io.pyle;

import java.io.IOException;

/**
 * This exception is thrown by {@link Pyle} when
 * an IO action is taken on a file opened in a mode
 * that does not support that action
 */
public class FileModeMismatchException extends IOException {
  public FileModeMismatchException(String s) {
  }
}
