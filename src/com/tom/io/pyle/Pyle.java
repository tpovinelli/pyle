package com.tom.io.pyle;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.net.URI;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author Thomas Povinelli
 * This is a File object (sublcass of {@link File})
 * based on how files in Python work
 * <p>
 * It implements similar methods to the Python file
 * to make writing and reading to files easier
 * <p>
 * It provides methods for {@link #write(String)} and
 * {@link #read(int)} and other similar methods for ease of use
 * <p>
 * Despite being a subclass of File, this class may open a
 * BufferedReader and/or BufferedWriter. Thus, there are many
 * places where it may throw an IOException. In addition,
 * it must be closed when it is no longer used.
 * <p>
 * It would possible make more sense for this to subclass one
 * of the classes with Buffered in its name to make it clear
 * that this opens files in this way, however, since
 * it does both reading and writing there is no suitable class
 * or interface to use for this so File is chosen.
 * <p>
 * Please be aware that this should not be used as an argument to
 * Scanner, BufferedReader, FileInput/OutputStream etc.
 * This class is entirely self contained and provided the mode is correct
 * and the permissions are valid, reading and writing can be done
 * by this class alone.
 * <p>
 * All of the IO is done by {@link BufferedReader} and/or {@link BufferedWriter}
 * objects wrapping {@link FileReader} and/or {@link FileWriter} objects
 * respectively
 * @see File
 * @see FileMode
 * @see BufferedReader
 * @see BufferedWriter
 * @see FileReader
 * @see FileWriter
 * @see Closeable
 * @see Flushable
 */
public class Pyle extends File implements Readable, Closeable, Flushable {
  private final FileMode mode;

  private BufferedReader reader;
  private BufferedWriter writer;

  public Pyle(String pathname, FileMode mode) throws IOException {
    super(pathname);
    this.mode = mode;
    init();
  }

  public Pyle(String parent, String child, FileMode mode) throws IOException {
    super(parent, child);
    this.mode = mode;
    init();
  }

  public Pyle(File parent, String child, FileMode mode) throws IOException {
    super(parent, child);
    this.mode = mode;
    init();
  }

  public Pyle(URI uri, FileMode mode) throws IOException {
    super(uri);
    this.mode = mode;
    init();
  }

  /**
   * Follows the exact same contract of {@link BufferedReader#read(char[], int, int)}
   * with the following exception:
   * passing null as cbuf is allowed. doing so will skip over
   * {@code off} chars and {@code len} chars using
   * {@link BufferedReader#skip(long)} where {@code off + len}
   * is passed to the {@code skip(long)} method. If
   * {@code null} is passed as {@code cbuf} then the result of
   * {@link BufferedReader#skip(long)} is returned, otherwise
   * the result of {@link BufferedReader#read(char[], int, int)}
   * is returned
   * <p>
   * Requires Pyle to be opened in a readable {@link FileMode}.
   * Use {@link #isOpenInReadableMode()} to check for readable see {@link FileMode}
   * for further explanation
   */
  public int read(char[] cbuf, int off, int len) throws IOException {
    checkReadable();
    if (cbuf == null) {
      return (int) reader.skip(off + len);
    } else {
      return reader.read(cbuf, off, len);
    }
  }

  /**
   * Follows the exact same contract of {@link BufferedWriter#write(char[], int, int)}
   * but requires Pyle to be opened in a writable {@link FileMode}
   */
  public void write(@NotNull char[] cbuf, int off, int len) throws IOException {
    checkWritable();
    writer.write(cbuf, off, len);
  }

  private void init() throws IOException {
    switch (mode) {
      case READ:
        reader = new BufferedReader(new FileReader(this));
        break;
      case WRITE:
        writer = new BufferedWriter(new FileWriter(this));
        break;
      case APPEND:
        writer = new BufferedWriter(new FileWriter(this, true));
        break;
      case READ_PLUS:
      case APPEND_PLUS:
        reader = new BufferedReader(new FileReader(this));
        writer = new BufferedWriter(new FileWriter(this, true));
        break;
      case WRITE_PLUS:
        reader = new BufferedReader(new FileReader(this));
        writer = new BufferedWriter(new FileWriter(this, false));
        break;
      default:
        throw new IllegalArgumentException("File mode " + mode + " unrecognized or not implemented");
    }
  }

  /**
   * Writes the given String to the file
   *
   * @param text the string to write to the file
   * @throws IOException if the BufferedWriter or FileWriter throws an IOException during writing
   */
  public void write(String text) throws IOException {
    checkWritable();
    writer.write(text);
  }

  /**
   * Ensures the file is opened in a writable mode and throws an exception if not
   *
   * @throws FileModeMismatchException if the file is not opened in a writable mode
   */
  private void checkWritable() throws FileModeMismatchException {
    if (writer == null) {
      throw new FileModeMismatchException("File is opened as " + mode + " and cannot be written");
    }
  }

  /**
   * Ensures the file is opened in a readable mode and throws an exception if not
   *
   * @throws FileModeMismatchException if the file is not opened in a readable mode
   */
  private void checkReadable() throws FileModeMismatchException {
    if (reader == null) {
      throw new FileModeMismatchException("File is opened as " + mode + " and cannot be read");
    }
  }

  /**
   * Flush the BufferedWriter. Does nothing if file not opened for writing
   *
   * @throws IOException if the {@link BufferedWriter#flush()} throws
   */
  public void flush() throws IOException {
    if (writer != null) writer.flush();
  }

  /**
   * Close the {@link BufferedReader} and {@link BufferedWriter}
   *
   * @throws IOException if either Buffered* throws
   */
  public void close() throws IOException {
    if (reader != null) reader.close();
    if (writer != null) writer.close();
  }

  /**
   * Read all lines of the file and return a list containing them.
   * This will consume the entire file
   *
   * @return a List of Strings of lines in the file
   * @throws IOException according to {@link BufferedReader#readLine()}
   */
  @NotNull
  public List<String> readLines() throws IOException {
    checkReadable();
    ArrayList<String> list = new ArrayList<>();
    String line;
    while ((line = reader.readLine()) != null) {
      list.add(line);
    }
    return list;
  }

  /**
   * Skips {@code lines} lines in the file. File must be readable {@link #checkReadable()}
   * <p>
   * This method returns the number of lines skipped. This value may be less than
   * {@code lines} if the file reaches the end before all the lines are skipped
   *
   * @param lines number of lines to skip
   * @return the number of lines skipped
   * @throws IOException according to {@link #readLine()}
   */
  public int skipLines(int lines) throws IOException {
    checkReadable();
    int count = 0;
    for (int i = 0; i < lines; i++) {
      if (readLine() != null) {
        count++;
      } else {
        break;
      }
    }
    return count;
  }

  /**
   * Returns a stream of lines using {@code Files#lines}.
   * <p>
   * This creates a new stream using the files lines
   * from the beginning to the end. This does <b>not</b>
   * change or effect the position of the reader through
   * the file
   * <p>
   * If you would like to stream the rest of the lines
   * in the file rather than starting from the start,
   * you could use {@link #readLines()} which returns a list
   * of remaining lines and then calling {@link List#stream()}
   *
   * @return a Stream of lines of the file from the beginning
   * @throws IOException according to {@link Files#lines(Path)}
   * @see Files#lines(Path)
   * @see Stream
   */
  public Stream<String> lines() throws IOException {
    checkReadable();
    return Files.lines(Paths.get(getAbsolutePath()));
  }

  /**
   * Returns a stream of lines using {@code Files#lines}.
   * <p>
   * This creates a new stream using the files lines
   * from the beginning to the end. This does <b>not</b>
   * change or effect the position of the reader through
   * the file
   * <p>
   * If you would like to stream the rest of the lines
   * in the file rather than starting from the start,
   * you could use {@link #readLines()} which returns a list
   * of remaining lines and then calling {@link List#stream()}
   *
   * @param cs the {@link Charset} used for the file
   * @return a Stream of lines of the file from the beginning
   * @throws IOException according to {@link Files#lines(Path)}
   * @see Files#lines(Path, Charset)
   * @see Stream
   */
  public Stream<String> lines(Charset cs) throws IOException {
    checkReadable();
    return Files.lines(Paths.get(getAbsolutePath()), cs);
  }

  /**
   * Write the text line and then a {@link System#lineSeparator()}
   *
   * @param line the text to be written
   * @throws IOException according to {@link BufferedWriter#write(String)} )}
   */
  public void writeLine(String line) throws IOException {
    write(line + System.lineSeparator());
  }

  /**
   * Writes the list of Strings to a file connecting each String in the list
   * with the value of {@link System#lineSeparator()}.
   * <p>
   * Specifically, it does a regular write using the result of
   * {@link String#join(CharSequence, CharSequence...)} using
   * {@link System#lineSeparator()} and lines
   *
   * @param lines the lines to be written
   * @throws IOException according to {@link BufferedWriter#write(String)}
   */
  public void writeLines(List<String> lines) throws IOException {
    write(String.join(System.lineSeparator(), lines));
  }

  /**
   * Read a single line from the file using {@link BufferedReader#readLine()}
   *
   * @return a String which is the next line of the file or null if there are no lines left
   * @throws IOException according to {@link BufferedReader#readLine()}
   */
  @Nullable
  public String readLine() throws IOException {
    checkReadable();
    return reader.readLine();
  }

  /**
   * Read bytes chars from the file
   *
   * @param bytes the number of characters to read from the file
   * @return a String containing the next bytes characters read from the file
   * @throws IOException according to {@link BufferedReader#read(char[], int, int)}
   */
  @NotNull
  public String read(int bytes) throws IOException {
    checkReadable();
    char[] buff = new char[bytes];
    int read = reader.read(buff, 0, bytes);
    if (read == -1) {
      return "";
    }
    return new String(buff, 0, read);

  }

  /**
   * Read all text from the file using successive calls to {@link #read(int)}
   * and a buffer size of 1024
   *
   * @return a String containing all the text in the file
   * @throws IOException according to {{@link #read(int)}}
   */
  @NotNull
  public String read() throws IOException {
    checkReadable();
    String s;
    StringBuilder result = new StringBuilder();
    while (!(s = read(1024)).equals("")) {
      result.append(s);
    }
    return result.toString();
  }

  /**
   * Return the file mode that this file was opened with
   *
   * @return the file mode of this file
   */
  public FileMode getMode() {
    return mode;
  }

  @Contract(pure = true)
  private boolean isOpenInWritableMode() {
    return mode == FileMode.READ_PLUS || mode == FileMode.WRITE ||
        mode == FileMode.WRITE_PLUS || mode == FileMode.APPEND ||
        mode == FileMode.APPEND_PLUS;
  }

  @Contract(pure = true)
  private boolean isOpenInReadableMode() {
    return mode == FileMode.READ || mode == FileMode.READ_PLUS ||
        mode == FileMode.WRITE_PLUS || mode == FileMode.APPEND_PLUS;
  }

  /**
   * Returns true if the file is able to be written to
   * <p>
   * In order for this to return true the file must be opened in a
   * writable mode and the file object itself returns true for the
   * call to {@link File#canWrite()}
   *
   * @return true if the file is writable and false otherwise
   * @see FileMode
   */
  public boolean isWritable() {
    return canWrite() && isOpenInWritableMode();
  }

  /**
   * Returns true if the file is able to be written to.
   * <p>
   * In order for this to return true the file must be opened
   * in a Readable mode and the file object itself must return
   * true for the method {@link File#canRead()}
   *
   * @return true if the file is readable
   * @see FileMode
   */
  public boolean isReadable() {
    return canRead() && isOpenInReadableMode();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int read(@NotNull CharBuffer cb) throws IOException {
    checkReadable();
    return reader.read(cb);
  }
}
