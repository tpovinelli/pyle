package com.tom.io.pyle;

/**
 * Indicates the mode that a {@link Pyle} object
 * has been opened in and the IO actions that it
 * can take because of that
 */
public enum FileMode {
  /**
   * This allows a com.tom.io.pyle.Pyle object to read from the
   * underlying file using the read methods
   * but writing is not permitted
   *
   * (readable)
   */
  READ,

  /**
   * This allows a com.tom.io.pyle.Pyle object to write to the
   * underlying file. The file will be created if it
   * does not exist and truncated to 0 if it does.
   *
   * Reading is not permitted in this mode
   *
   * (writable)
   */
  WRITE,

  /**
   * This mode is like {@link #WRITE} but it does
   * not perform a truncation. The writing begins
   * at the end of the file as it was before being
   * opened
   *
   * (writable)
   */
  APPEND,

  /**
   * This mode is like read, but it also allows
   * writing to the file. The writing takes
   * place at the end of the file
   * and the reading at the beginning.
   * File is not truncated
   *
   * (readable, writable)
   */
  READ_PLUS,

  /**
   * This mode is like write, but reading is allowed
   * Truncation happens, as in write, and so
   * reading and writing is at the beginning of the file
   *
   * (readable, writable)
   */
  WRITE_PLUS,

  /**
   * This mode is the same as {@link #READ_PLUS}
   *
   * (readable, writable)
   */
  APPEND_PLUS
}
